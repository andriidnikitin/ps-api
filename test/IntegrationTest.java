import com.avaje.ebean.Ebean;
import models.ModelMock;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;
import services.ServiceMock;
import services.exceptions.ParkingStarDatabaseException;
import services.exceptions.ParkingStarException;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;

public class IntegrationTest {

    public static FakeApplication app;
    public static String createDdl = "";
    public static String dropDdl = "";

    @BeforeClass
    public static void startApp() throws IOException {
        app = fakeApplication(inMemoryDatabase());
        Helpers.start(app);

        // Reading the evolution file
        String evolutionContent = FileUtils.readFileToString(
                app.getWrappedApplication().getFile("conf/evolutions/default/1.sql"));

        // Splitting the String to get Create & Drop DDL
        String[] splittedEvolutionContent = evolutionContent.split("# --- !Ups");
        String[] upsDowns = splittedEvolutionContent[1].split("# --- !Downs");
        createDdl = upsDowns[0];
        dropDdl = upsDowns[1];

    }

    @AfterClass
    public static void stopApp() {
        Helpers.stop(app);
    }

    @Before
    public void createCleanDb() {
        Ebean.execute(Ebean.createCallableSql(dropDdl));
        Ebean.execute(Ebean.createCallableSql(createDdl));
    }

    @Test
    public void repositoryInitTest() {
        ServiceMock repository = new ServiceMock();
        try {
            repository.getAll();
            fail();
        } catch (ParkingStarException e) {
            assertEquals(e.getMessage(), ParkingStarDatabaseException.NO_RECORDS_PROVIDED);
        }
        assertEquals(repository.count(), 0);
    }


    @Test
    public void repositoryCreateTest() {
        ServiceMock repository = new ServiceMock();
        ModelMock record = new ModelMock();
        String fieldValue = "val1";
        int invalidId = record.getId();
        record.setName(fieldValue);
        long records = repository.count();
        ModelMock persistedRecord = null;
        try {
            persistedRecord = repository.create(record);
        } catch (ParkingStarException e) {
            fail();
        }
        assertTrue(repository.exists(persistedRecord.getId()));
        assertEquals(repository.count() - records, 1);
        assertNotNull(persistedRecord);
        assertEquals(persistedRecord, record);
        assertEquals(fieldValue, persistedRecord.getName());
        assertNotEquals(invalidId, persistedRecord.getId());
    }

    @Test
    public void repositoryReadTest() {
        ServiceMock repository = new ServiceMock();
        ModelMock record = new ModelMock();
        record.setName("val1");
        ModelMock persistedRecord = null;
        ModelMock queriedRecord = null;
        try {
            persistedRecord = repository.create(record);
            queriedRecord = repository.read(persistedRecord.getId());
        } catch (ParkingStarException e) {
            fail();
        }
        assertNotNull(queriedRecord);
        assertEquals(queriedRecord, persistedRecord);
        assertEquals(queriedRecord.getName(), persistedRecord.getName());
    }

    @Test
    public void repositoryUpdateTest() {
        ServiceMock repository = new ServiceMock();
        ModelMock record = new ModelMock();
        String fieldValue = "val2";
        String newFieldValue = "val3";

        record.setName(fieldValue);
        try {
            record = repository.create(record);
        } catch (ParkingStarException e) {
            fail();
        }
        int id = record.getId();

        ModelMock copyRecord = new ModelMock();
        copyRecord.setId(id);
        copyRecord.setName(newFieldValue);

        ModelMock updatedRecord = null;
        try {
            repository.update(id, copyRecord);
            updatedRecord = repository.read(id);
        } catch (ParkingStarException e) {
            fail();
        }

        assertNotEquals(updatedRecord.getName(), fieldValue);
        assertEquals(updatedRecord.getName(), newFieldValue);
    }

    @Test
    public void repositoryDeleteTest() {
        ServiceMock repository = new ServiceMock();
        ModelMock record = new ModelMock();
        long records = repository.count();
        ModelMock persistedRecord = null;
        try {
            persistedRecord = repository.create(record);
            repository.delete(persistedRecord.getId());
        } catch (ParkingStarException e) {
            fail();
        }

        assertFalse(repository.exists(persistedRecord.getId()));
        assertTrue(records == repository.count());
    }

    @Test
    public void repositoryGetAllTest() {
        ServiceMock repository = new ServiceMock();
        long records = repository.count();
        List<ModelMock> all = null;
        try {
            repository.create(new ModelMock());
            repository.create(new ModelMock());
            repository.create(new ModelMock());
            all = repository.getAll();
        } catch (ParkingStarException e) {
            fail();
        }
        assertNotNull(all);
        assertEquals(all.size() - records, 3);
    }

    @Test
    public void repositoryDeleteAllTest() {
        ServiceMock repository = new ServiceMock();
        long records = repository.count();
        ModelMock newRecord = null;
        try {
            newRecord = repository.create(new ModelMock());
            repository.create(new ModelMock());
            repository.create(new ModelMock());
        } catch (ParkingStarException e) {
            e.printStackTrace();
        }
        repository.deleteAll();

        assertFalse(repository.exists(newRecord.getId()));
        assertTrue(records == repository.count());
    }
}