package services;

import models.ModelMock;
import services.ebean.BaseEBeanService;

public class ServiceMock extends BaseEBeanService<ModelMock, Integer> {
    @Override
    protected boolean recordExists(ModelMock record) {
        return false;
    }

    @Override
    protected Integer getId(ModelMock record) {
        return record.getId();
    }

    @Override
    protected Class<ModelMock> getEntityClass() {
        return ModelMock.class;
    }
}
