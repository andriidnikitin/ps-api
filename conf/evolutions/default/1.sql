# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table image (
  record_id                 integer not null,
  id                        varchar(255),
  constraint pk_image primary key (record_id))
;

create table news (
  id                        integer not null,
  title                     varchar(255),
  image_id                  integer,
  content                   varchar(255),
  date_of_creating          bigint,
  constraint uq_news_image_id unique (image_id),
  constraint pk_news primary key (id))
;

create table nomination (
  id                        integer not null,
  user_id                   integer,
  violation_code            integer,
  gotcha                    boolean,
  longitude                 double,
  latitude                  double,
  image_id                  integer,
  violator_id               integer,
  description               varchar(255),
  timestamp                 timestamp,
  admin_area                varchar(255),
  sub_admin_area            varchar(255),
  country_name              varchar(255),
  locality                  varchar(255),
  sub_locality              varchar(255),
  premises                  varchar(255),
  thoroughfare              varchar(255),
  sub_thoroughfare          varchar(255),
  constraint uq_nomination_image_id unique (image_id),
  constraint pk_nomination primary key (id))
;

create table user (
  id                        integer not null,
  email                     varchar(255),
  login                     varchar(255),
  name                      varchar(255),
  password                  varchar(255),
  token                     varchar(255),
  image_id                  integer,
  score                     bigint,
  creation_date             timestamp,
  constraint uq_user_email unique (email),
  constraint uq_user_login unique (login),
  constraint uq_user_image_id unique (image_id),
  constraint pk_user primary key (id))
;

create table violator (
  violator_id               integer not null,
  car_number                varchar(255),
  car_color                 varchar(255),
  car_brand                 varchar(255),
  car_model                 varchar(255),
  constraint uq_violator_car_number unique (car_number),
  constraint pk_violator primary key (violator_id))
;

create sequence image_seq;

create sequence news_seq;

create sequence nomination_seq;

create sequence user_seq;

create sequence violator_seq;

alter table news add constraint fk_news_titleImage_1 foreign key (image_id) references image (record_id) on delete restrict on update restrict;
create index ix_news_titleImage_1 on news (image_id);
alter table nomination add constraint fk_nomination_user_2 foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_nomination_user_2 on nomination (user_id);
alter table nomination add constraint fk_nomination_image_3 foreign key (image_id) references image (record_id) on delete restrict on update restrict;
create index ix_nomination_image_3 on nomination (image_id);
alter table nomination add constraint fk_nomination_violator_4 foreign key (violator_id) references violator (violator_id) on delete restrict on update restrict;
create index ix_nomination_violator_4 on nomination (violator_id);
alter table user add constraint fk_user_image_5 foreign key (image_id) references image (record_id) on delete restrict on update restrict;
create index ix_user_image_5 on user (image_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists image;

drop table if exists news;

drop table if exists nomination;

drop table if exists user;

drop table if exists violator;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists image_seq;

drop sequence if exists news_seq;

drop sequence if exists nomination_seq;

drop sequence if exists user_seq;

drop sequence if exists violator_seq;

