package models;

public class ViolatorScore {

    private Violator violator;
    private Image image;
    private int violationsCount;
    private long lastActivityTime;

    public Violator getViolator() {
        return violator;
    }

    public void setViolator(Violator violator) {
        this.violator = violator;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getViolationsCount() {
        return violationsCount;
    }

    public void setViolationsCount(int violationsCount) {
        this.violationsCount = violationsCount;
    }

    public long getLastActivityTime() {
        return lastActivityTime;
    }

    public void setLastActivityTime(long lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }
}
