package models;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
public class News extends Model {

    @Id
    @GeneratedValue
    private int id;
    private String title;
    @OneToOne
    @JoinColumn(name = "image_id")
    private Image titleImage;
    private String content;
    private long dateOfCreating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getDateOfCreating() {
        return dateOfCreating;
    }

    public void setDateOfCreating(long dateOfCreating) {
        this.dateOfCreating = dateOfCreating;
    }

    public Image getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(Image titleImage) {
        this.titleImage = titleImage;
    }
}
