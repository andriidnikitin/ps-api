package models;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
public class Image extends Model {

    private String id;

    @Id
    @GeneratedValue
    private int recordId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }
}
