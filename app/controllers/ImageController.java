package controllers;

import di.SpringService;
import models.Image;
import play.mvc.Http;
import play.mvc.Result;
import services.ImageService;
import services.exceptions.ParkingStarException;

import javax.validation.constraints.NotNull;

public class ImageController extends CrudController<Image, String> {

    public Result get(String name) {
        try {
            return ok(getService().loadImage(name));
        } catch (ParkingStarException e) {
            return badRequest(e.getMessage());
        }
    }

    @Override
    protected ImageService getService() {
        return SpringService.getService(ImageService.class);
    }

    @Override
    protected Class<Image> getBeanClass() {
        return Image.class;
    }

    @NotNull
    @Override
    protected Image bindBean() throws ParkingStarException {
        Image record = new Image();
        record.setId(getAbsolutePath());
        return record;
    }

    private String getAbsolutePath() {
        Http.RequestBody body = request().body();
        if (body == null) return null;
        return body.asRaw().asFile().getAbsolutePath();
    }
}
