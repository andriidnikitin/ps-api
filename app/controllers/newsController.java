package controllers;

import di.SpringService;
import models.News;
import play.mvc.Result;
import services.NewsService;
import services.exceptions.ParkingStarException;

import static play.libs.Json.toJson;

public class NewsController extends CrudController<News, Integer> {

    @Override
    protected NewsService getService() {
        return SpringService.getService(NewsService.class);
    }

    @Override
    protected Class<News> getBeanClass() {
        return News.class;
    }

    public Result getAll(Integer userId) {
        try {
            return ok(toJson(getService().getAll(userId)));
        } catch (ParkingStarException e) {
            return notFound(e.getMessage());
        }
    }
}
