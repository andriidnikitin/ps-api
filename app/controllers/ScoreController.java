package controllers;

import di.SpringService;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.ScoreService;
import services.exceptions.ParkingStarException;

import static play.libs.Json.toJson;

public class ScoreController extends Controller {

    public Result getViolatorNominationsCount(int violatorId) {
        try {
            return ok(toJson(getService().getViolatorNominationsCount(violatorId)));
        } catch (ParkingStarException e) {
            return notFound(e.getMessage());
        }
    }

    public Result getViolatorNominations(int violatorId) {
        try {
            return ok(toJson(getService().getViolatorNominations(violatorId)));
        } catch (ParkingStarException e) {
            return notFound(e.getMessage());
        }
    }

    public Result getViolatorImage(int violatorId) {
        try {
            return ok(Json.toJson(getService().getViolatorImage(violatorId)));
        } catch (ParkingStarException e) {
            return notFound(e.getMessage());
        }
    }

    public Result getScores() {
        try {
            return ok(Json.toJson(getService().getScores()));
        } catch (ParkingStarException e) {
            return notFound(e.getMessage());
        }
    }

    public Result getNominators(int violatorId) {
        try {
            return ok(Json.toJson(getService().getNominators(violatorId)));
        } catch (ParkingStarException e) {
            return notFound(e.getMessage());
        }
    }

    private ScoreService getService(){
        return SpringService.getService(ScoreService.class);
    }
}
