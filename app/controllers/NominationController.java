package controllers;

import di.SpringService;
import models.Image;
import models.Nomination;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import services.NominationService;
import services.exceptions.ParkingStarException;

import java.io.IOException;

public class NominationController extends CrudController<Nomination, Integer> {

    @Override
    protected NominationService getService() {
        return SpringService.getService(NominationService.class);
    }

    @Override
    protected Class<models.Nomination> getBeanClass() {
        return models.Nomination.class;
    }

    @Override
    public Result add() {
        Http.MultipartFormData requestData = request().body().asMultipartFormData();
        try {
            Image image = new Image();
            image.setId(requestData.getFile("photo").getFile().getAbsolutePath());
            String json = requestData.asFormUrlEncoded().get("nomination")[0];
            Nomination nomination = getObjectMapper().readValue(json, getBeanClass());
            nomination.setImage(image);
            return ok(Json.toJson(getService().create(nomination)));
        } catch (ParkingStarException e1) {
            return badRequest(e1.getMessage());
        } catch (IOException | NullPointerException  e1) {
            return badRequest("unable to create new record");
        }
    }
}
