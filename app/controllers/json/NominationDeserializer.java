package controllers.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Nomination;
import models.User;
import models.Violator;

import java.io.IOException;
import java.util.Date;

public class NominationDeserializer extends JsonDeserializer<Nomination> {


    @Override
    public Nomination deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        JsonNode violatorNode = node.get("violator");

        Nomination nomination = new Nomination();
        nomination.setId(node.get("id").asInt());
        nomination.setViolationCode(node.get("violationCode").asInt());
        nomination.setGotcha(node.get("gotcha").asBoolean());
        nomination.setDescription(node.get("description").asText());
        nomination.setTimestamp(new ObjectMapper().readValue(node.get("timestamp").asText(), Date.class));
        nomination.setLongitude(node.get("longitude").asDouble());
        nomination.setLatitude(node.get("latitude").asDouble());
        nomination.setCountryName(node.get("countryName").asText());
        nomination.setAdminArea(node.get("adminArea").asText());
        nomination.setSubAdminArea(node.get("subAdminArea").asText());
        nomination.setLocality(node.get("locality").asText());
        nomination.setSubLocality(node.get("subLocality").asText());
        nomination.setPremises(node.get("premises").asText());
        nomination.setThoroughfare(node.get("thoroughfare").asText());
        nomination.setSubThoroughfare(node.get("subThoroughfare").asText());

        User user = new User();
        user.setId(node.get("userId").asInt());
        nomination.setUser(user);

        Violator violator = new Violator();
        violator.setId(violatorNode.get("id").asInt());
        violator.setCarColor(violatorNode.get("carColor").asText("unknown").trim());
        violator.setCarBrand(violatorNode.get("carBrand").asText("unknown").trim());
        violator.setCarModel(violatorNode.get("carModel").asText("unknown").trim());
        violator.setCarNumber(violatorNode.get("carNumber").asText(" ").trim());
        nomination.setViolator(violator);

        return nomination;
    }
}