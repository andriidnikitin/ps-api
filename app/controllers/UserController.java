package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import di.SpringService;
import models.User;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import services.UserService;
import services.exceptions.ParkingStarAuthrizationException;
import services.exceptions.ParkingStarException;

public class UserController extends CrudController<User, Integer> {

    @Override
    protected UserService getService() {
        return SpringService.getService(UserService.class);
    }

    @Override
    protected Class<User> getBeanClass() {
        return User.class;
    }

    public Result updateAvatar(Integer id) {
        Http.MultipartFormData requestData = request().body().asMultipartFormData();
        try {
            String newPath = requestData.getFile("photo").getFile().getAbsolutePath();
            getService().updateAvatar(id, newPath);
            return ok();
        } catch (ParkingStarException | NullPointerException e1) {
            return badRequest(e1.getMessage());
        }
    }

    public Result login(String login) {
        try {
            JsonNode jsonNode = request().body().asJson();
            if (jsonNode == null)
                throw new ParkingStarAuthrizationException(ParkingStarAuthrizationException.PASSWORD_NOT_MATCH);
            return ok(Json.toJson(getService().read(login, jsonNode.asText())));
        } catch (ParkingStarException e) {
            return badRequest(e.getMessage());
        }
    }

    @Override
    protected User bindBean() throws ParkingStarException {
        User user = super.bindBean();
        if (user.getName() == null || user.getEmail() == null || user.getLogin() == null || user.getPassword() == null)
            throw new IllegalStateException("invalid json");
        return user;
    }
}
