package controllers;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import controllers.json.NominationDeserializer;
import models.Nomination;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import services.IService;
import services.exceptions.ParkingStarDatabaseException;
import services.exceptions.ParkingStarException;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;

import static play.libs.Json.toJson;

public abstract class CrudController<M extends Model, ID extends Serializable> extends Controller {

    private ObjectMapper objectMapper = createObjectMapper();

    public Result add() {
        try {
            M bean = bindBean();
            bean = getService().create(bean);
            return ok(Json.toJson(bean));
        } catch (ParkingStarException e) {
            return badRequest(e.getMessage());
        }
    }

    public Result get(ID id) {
        try {
            return ok(toJson(getService().read(id)).toString());
        } catch (ParkingStarException e) {
            return notFound(e.getMessage());
        }
    }

    public Result getAll() {
        try {
            return ok(toJson(getService().getAll()));
        } catch (ParkingStarException e) {
            return notFound(e.getMessage());
        }
    }

    public Result update(ID id) {
        try {
            getService().update(id, bindBean());
            return ok();
        } catch (ParkingStarException e) {
            return badRequest(e.getMessage());
        }
    }

    public Result delete(ID id) {
        try {
            getService().delete(id);
            return ok();
        } catch (ParkingStarException e) {
            return notFound(e.getMessage());
        }
    }

    /**
     * This method extracts instance of model from body of request.
     * @return new instance, received from request body.
     * @throws ParkingStarException
     */
    @BodyParser.Of(BodyParser.Json.class)
    @NotNull
    protected M bindBean() throws ParkingStarException {
        try {
            return request().body().asText() == null
                    ? getObjectMapper().readValue(request().body().asJson().toString(), getBeanClass()) :
                    getObjectMapper().readValue(request().body().asText(), getBeanClass());
        } catch (IOException | NullPointerException e1) {
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.INVALID_RECORD);
        }
    }

    protected ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    /**
     * this method defines object mapper, used for mapping request bodies. Override this method to register new modules and other setups.
     * @return new configured ObjectMapper instance.
     */
    protected ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule("SimpleModule",
                new Version(1,0,0,null));
        simpleModule.addDeserializer(Nomination.class, new NominationDeserializer());
        objectMapper.registerModule(simpleModule);
        return objectMapper;
    }

    protected abstract <T extends IService<M, ID>> T getService();

    protected abstract Class<M> getBeanClass();
}
