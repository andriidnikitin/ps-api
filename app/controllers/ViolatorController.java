package controllers;

import di.SpringService;
import models.Violator;
import play.libs.Json;
import play.mvc.Result;
import services.ViolatorService;
import services.exceptions.ParkingStarException;

public class ViolatorController extends CrudController<Violator, Integer> {

    @Override
    protected ViolatorService getService() {
        return SpringService.getService(ViolatorService.class);
    }

    @Override
    protected Class<Violator> getBeanClass() {
        return Violator.class;
    }

    public Result getNominations(int id) {
        try {
            return ok(Json.toJson(getService().getNominations(id)));
        } catch (ParkingStarException e) {
            return badRequest(e.getMessage());
        }
    }
}
