package services;

import models.Nomination;

public interface NominationService extends IService<Nomination, Integer> {
    Nomination findLastByViolatorId(int id);
}
