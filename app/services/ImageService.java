package services;

import models.Image;
import services.exceptions.ParkingStarException;

import java.io.File;

public interface ImageService extends IService<Image, String> {

    File loadImage(String name) throws ParkingStarException;
}
