package services;

import services.exceptions.ParkingStarException;

import java.io.Serializable;
import java.util.List;

public interface IService<M, ID extends Serializable> {

    M create(M record) throws ParkingStarException;

    M read(ID recordId) throws ParkingStarException;

    Iterable<M> getAll() throws ParkingStarException;

    void update(ID recordId, M record) throws ParkingStarException;

    void delete(ID recordId) throws ParkingStarException;

    <F> List<M> findByField(String fieldName, F value) throws ParkingStarException;

    <F> int countByField(String fieldName, F value);
}
