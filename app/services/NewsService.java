package services;

import models.News;
import services.exceptions.ParkingStarException;

import java.util.List;

public interface NewsService extends IService<News, Integer> {
    List<News> getAll(int userId) throws ParkingStarException;
}
