package services;

import models.User;
import services.exceptions.ParkingStarException;

public interface UserService extends IService<User, Integer> {
    int ADD_NOMINATION_CODE = 1;
    int DELETE_NOMINATION_CODE = 2;
    int ADD_NOMINATION_JAILBIRD_CODE = 3;

    User read(String login, String password) throws ParkingStarException;

    void recalculatePoints(int userId, int occasion);

    void updateAvatar(Integer id, String newPath) throws ParkingStarException;
}
