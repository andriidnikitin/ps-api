package services.ebean;

import com.avaje.ebean.Model;
import services.IService;
import services.exceptions.ParkingStarDatabaseException;
import services.exceptions.ParkingStarException;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public abstract class BaseEBeanService<M extends Model, ID extends Serializable>
        implements IService<M, ID> {


    protected final Model.Finder<ID, M> finder = createFinder();

    public boolean exists(ID id) {
        return finder.byId(id) != null;
    }

    public long count() {
        return finder.findRowCount();
    }

    public void deleteAll() {
        finder.all().forEach(M::delete);
    }

    @Override
    public M create(M record) throws ParkingStarException {
        if (record == null) {
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.INVALID_RECORD);
        }
        if (recordExists(record))
        throw new ParkingStarDatabaseException(ParkingStarDatabaseException.DUPLICATION);
        try {
            record.insert();
        } catch (PersistenceException e) {
            throw new IllegalStateException(e.getCause() + ";" + e.getMessage());
            //throw new ParkingStarDatabaseException(ParkingStarDatabaseException.DUPLICATION);
        }
        return record;
    }

    @Override
    public M read(ID recordId) throws ParkingStarException {
        M record = finder.byId(recordId);
        if (record == null) {
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.RECORD_NOT_FOUND, recordId);
        }
        return record;
    }

    @Override
    public void update(ID recordId, M record) throws ParkingStarException {
        if (record == null) {
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.INVALID_RECORD, recordId);
        }
        if (!Objects.equals(getId(record), recordId))
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.ID_NOT_MATCH, recordId);
        record.update();
    }

    @Override
    public void delete(ID recordId) throws ParkingStarException {
        M bean = finder.byId(recordId);
        if (bean == null)
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.RECORD_NOT_FOUND, recordId);
        bean.delete();
    }

    @Override
    public List<M> getAll() throws ParkingStarException {
        List<M> all = finder.all();
        if (all == null || all.isEmpty()) {
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.NO_RECORDS_PROVIDED);
        }
        return all;
    }

    @Override
    public <F> List<M> findByField(String fieldName, F value) {
        return finder.where().eq(fieldName, value).findList();
    }

    @Override
    public <F> int countByField(String fieldName, F value) {
        return finder.where().eq(fieldName, value).findRowCount();
    }

    protected Model.Finder<ID, M> createFinder() {
        return new Model.Finder<>(getEntityClass());
    }

    protected abstract ID getId(M record);

    protected abstract Class<M> getEntityClass();

    /**
     * this method checks database for records that are duplicating (according to the business logic) provided record.
     * @param record is record that will be checked for duplications
     * @return true if record is duplicated
     */
    protected abstract boolean recordExists(M record);
    //FIXME replace with compound primary keys using @Embedded (object, that stores multiple primary keys: https://docs.jboss.org/ejb3/docs/tutorial/1.0.7/html/Composite_Primary_Keys_for_Entities_in_EJB3.html)

}