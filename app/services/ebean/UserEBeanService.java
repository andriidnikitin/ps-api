package services.ebean;

import di.SpringService;
import models.Image;
import models.User;
import org.springframework.stereotype.Repository;
import services.ImageService;
import services.UserService;
import services.exceptions.ParkingStarAuthrizationException;
import services.exceptions.ParkingStarDatabaseException;
import services.exceptions.ParkingStarException;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class UserEBeanService extends BaseEBeanService<User, Integer> implements UserService {

    private static final int POINTS_FOR_ADDING = 5;
    private static final int POINTS_FOR_JAILBIRD = 3;

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    @Override
    protected Integer getId(User record) {
        return record.getId();
    }


    @Override
    public User read(String login, String password) throws ParkingStarException {
        List<User> users = findByField("login", login);
        if (users.size() < 1 || users.get(0) == null)
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.RECORD_NOT_FOUND, "(login) " + login);
        if (Objects.equals(password, users.get(0).getPassword()))
            return users.get(0);
        else throw new ParkingStarAuthrizationException(ParkingStarAuthrizationException.PASSWORD_NOT_MATCH);
    }

    /**
     * this method is being called when user's account points are being modified.
     *
     * @param userId   is ID of user
     * @param occasion is code of reason for adding points
     */
    @Override
    public void recalculatePoints(int userId, int occasion) {
        User record = finder.byId(userId);
        switch (occasion) {
            case UserService.ADD_NOMINATION_CODE:
                record.setScore(record.getScore() + POINTS_FOR_ADDING);
                break;
            case UserService.ADD_NOMINATION_JAILBIRD_CODE:
                record.setScore(record.getScore() + POINTS_FOR_ADDING + POINTS_FOR_JAILBIRD);
                break;
            case UserService.DELETE_NOMINATION_CODE:
                record.setScore(record.getScore() - POINTS_FOR_ADDING);
                break;
            default:
                break;
        }
        record.update();
    }

    @Override
    public void updateAvatar(Integer id, String newPath) throws ParkingStarException {
        Image oldRecord = read(id).getImage();
        Image newRecord = new Image();
        newRecord.setId(newPath);
        SpringService.getService(ImageService.class).update(oldRecord.getId(), newRecord);
    }

    @Override
    public User create(User bean) throws ParkingStarException {
        if (bean == null || bean.getLogin() == null || bean.getPassword() == null || bean.getEmail() == null) {
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.INVALID_RECORD);
        }
        if (bean.getImage() == null) {
            Image image = SpringService.getService(ImageService.class).create(new Image());
            if (image == null)
                throw new ParkingStarDatabaseException(ParkingStarDatabaseException.DATABASE_ERROR);
            bean.setImage(image);
        }
        bean.setCreationDate(new Date());
        return super.create(bean);
    }

    @Override
    public void update(Integer recordId, User record) throws ParkingStarException {
        User oldRecord = read(recordId);
        String newPassword = record.getPassword();
        String newName = record.getName();
        if (newPassword != null) oldRecord.setPassword(newPassword);
        if (newName != null) oldRecord.setName(newName);

        super.update(recordId, oldRecord);
    }

    @Override
    protected boolean recordExists(User record) {
        List<User> existingUsers = findByField("login", record.getLogin());
        existingUsers.addAll(findByField("email", record.getEmail()));
        return (existingUsers.size() > 0);
    }

    /**
     * this method returns public info for all users
     *
     * @return all accounts (only public) info
     * @throws ParkingStarException
     */
    @Override
    public List<User> getAll() throws ParkingStarException {
        List<User> all = super.getAll();
        for (int i = 0; i < all.size(); i++)
            all.set(i, hideSensitiveData(all.get(i)));
        return all;
    }

    private User hideSensitiveData(User read) {
        read.setEmail(null);
        read.setPassword(null);
        read.setName(null);
        read.setToken(null);
        return read;
    }
}