package services.ebean;

import di.SpringService;
import models.Nomination;
import models.Violator;
import org.springframework.stereotype.Repository;
import services.NominationService;
import services.ViolatorService;
import services.exceptions.ParkingStarException;

import java.util.List;

@Repository
public class ViolatorEBeanService extends BaseEBeanService<Violator, Integer> implements ViolatorService {

    @Override
    public Violator create(Violator record) throws ParkingStarException {
        List<Violator> existingViolators = findByField("car_number", record.getCarNumber());
        if (existingViolators.size() > 0) {
            Violator violator = existingViolators.get(0);
            record.setId(violator.getId());
            record.update();
            return record;
        }
        return super.create(record);
    }

    @Override
    public Violator read(Integer recordId) throws ParkingStarException {
        cleanUp();
        return super.read(recordId);
    }

    @Override
    public List<Violator> getAll() throws ParkingStarException {
        cleanUp();
        return super.getAll();
    }

    private void cleanUp() throws ParkingStarException {
        ViolatorService service = SpringService.getService(ViolatorService.class);
        List<Violator> all = super.getAll();
        for (Violator violator : all) {
            if (service.getNominations(violator.getId()).size() < 1)
                violator.delete();
        }
    }

    @Override
    protected boolean recordExists(Violator record) {
        return false;
    }

    @Override
    public List<Nomination> getNominations(int violatorId) throws ParkingStarException {
        return SpringService.getService(NominationService.class).findByField("violator_id", violatorId);
    }

    @Override
    protected Class<Violator> getEntityClass() {
        return Violator.class;
    }

    @Override
    protected Integer getId(Violator record) {
        return record.getId();
    }
}