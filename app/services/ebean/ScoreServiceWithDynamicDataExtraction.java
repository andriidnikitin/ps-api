package services.ebean;

import di.SpringService;
import models.*;
import org.springframework.stereotype.Repository;
import services.NominationService;
import services.ScoreService;
import services.ViolatorService;
import services.exceptions.ParkingStarException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ScoreServiceWithDynamicDataExtraction implements ScoreService {

    @Override
    public int getViolatorNominationsCount(int id) throws ParkingStarException {
        return getNominationService().countByField(Violator.ID_FIELD_NAME, id);
    }

    @Override
    public List<Nomination> getViolatorNominations(int id) throws ParkingStarException {
        return getNominationService().findByField(Violator.ID_FIELD_NAME, id);
    }

    @Override
    public Image getViolatorImage(int id) throws ParkingStarException {
        Nomination lastByViolatorId = getNominationService().findLastByViolatorId(id);
        return lastByViolatorId == null ? null : lastByViolatorId.getImage();
    }

    @Override
    public List<ViolatorScore> getScores() throws ParkingStarException {
        List<ViolatorScore> scores = new ArrayList<>();
        Iterable<Violator> violators = getViolatorService().getAll();
        for (Violator violator : violators){
            int violatorId = violator.getId();
            ViolatorScore score = new ViolatorScore();
            Nomination lastNomination = getNominationService().findLastByViolatorId(violatorId);
            score.setLastActivityTime(lastNomination.getTimestamp().getTime());
            score.setViolator(violator);
            score.setImage(getViolatorImage(violatorId));
            score.setViolationsCount(getViolatorNominationsCount(violatorId));
            scores.add(score);
        }
        return scores;
    }

    @Override
    public List<String> getNominators(int violatorId) throws ParkingStarException {
        return getNominationService().findByField("violator_id", violatorId).stream().map(Nomination::getUser).map(User::getLogin).collect(Collectors.toList());
    }

    private NominationService getNominationService(){
        return SpringService.getService(NominationService.class);
    }

    private ViolatorService getViolatorService(){
        return SpringService.getService(ViolatorService.class);
    }
}