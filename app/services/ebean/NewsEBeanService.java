package services.ebean;

import di.SpringService;
import models.Image;
import models.News;
import models.User;
import org.springframework.stereotype.Repository;
import services.NewsService;
import services.UserService;
import services.exceptions.ParkingStarException;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Repository
public class NewsEBeanService extends BaseEBeanService<News, Integer> implements NewsService {

    private static final String LINK = "http://www.youtube.com/watch?v=dQw4w9WgXcQ";
    public static final String PROMPT = "Welcome to our service - ParkingStar! To introduce you our features, go to overview and a short lesson here: ";

    @Override
    protected Class<News> getEntityClass() {
        return News.class;
    }

    @Override
    protected boolean recordExists(News record) {
        return findByField("title", record.getTitle()).size() > 0;
    }

    @Override
    protected Integer getId(News record) {
        return record.getId();
    }

    @Override
    public List<News> getAll(int userId) throws ParkingStarException {
        List<News> newsletter;
        try {
            newsletter = super.getAll();
        } catch (ParkingStarException e) {
            newsletter = new ArrayList<>();
        }
        newsletter.addAll(generateWelcomeNews(SpringService.getService(UserService.class).read(userId)));
        return newsletter;
    }

    @NotNull
    private List<News> generateWelcomeNews(User user) throws ParkingStarException {
        Date creationDate = user.getCreationDate();
        String login = user.getLogin();

        Image titleImage = new Image();
        titleImage.setRecordId(0);
        titleImage.setId("logo.png");

        News welcome = new News();
        welcome.setDateOfCreating(creationDate.getTime());
        welcome.setId(Integer.MAX_VALUE - 1);
        welcome.setTitleImage(titleImage);
        welcome.setTitle("Welcome, " + login + "!");
        welcome.setContent(PROMPT + LINK);

        List<News> welcomeNews = new ArrayList<>();
        welcomeNews.add(welcome);
        return welcomeNews;
    }
}