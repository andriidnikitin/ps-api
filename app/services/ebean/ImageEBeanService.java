package services.ebean;

import com.avaje.ebean.Model;
import models.Image;
import org.springframework.stereotype.Repository;
import play.Play;
import services.ImageService;
import services.exceptions.ParkingStarDatabaseException;
import services.exceptions.ParkingStarException;
import services.exceptions.ParkingStarFileException;

import javax.persistence.PersistenceException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.Objects;

@Repository
public class ImageEBeanService extends BaseEBeanService<Image, String> implements ImageService {

    @Override
    public Image create(Image image) throws ParkingStarException {
        String newPath = new Date().getTime() + "" + image.getRecordId() + ".jpg";
        if (image.getId() == null) {
            image.setId(newPath);
        }
        try {
            image = super.create(image);
        } catch (PersistenceException e) {
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.DUPLICATION, image.getId());
        }
        String oldPath = image.getId();
        if (!Objects.equals(oldPath, newPath)) {
            image.setId(newPath);
            image.update();
            moveFile(oldPath, getImageFilenameById(image.getId()));
        }
        return image;
    }

    @Override
    protected boolean recordExists(Image record) {
        return findByField("id", record.getId()).size() > 0;
    }

    @Override
    public void update(String oldId, Image image) throws ParkingStarException {
        moveFile(image.getId(), getImageFilenameById(oldId));
    }

    @Override
    public void delete(String id) throws ParkingStarException {
        try {
            Files.delete(new File(id).toPath());
            super.delete(id);
        } catch (NoSuchFileException e0) {
            throw new ParkingStarFileException(ParkingStarFileException.FILE_NOT_FOUND);
        } catch (IOException e1) {
            throw new ParkingStarFileException(ParkingStarFileException.UNABLE_TO_DELETE);
        }
    }

    @Override
    protected String getId(Image record) {
        return record.getId();
    }

    @Override
    protected Class<Image> getEntityClass() {
        return Image.class;
    }

    @Override
    public File loadImage(String id) throws ParkingStarException {
        File file = new File(getImageFilenameById(id));
        if (!file.exists())
            throw new ParkingStarFileException(ParkingStarFileException.FILE_NOT_FOUND);
        return file;
    }

    @Override
    protected Model.Finder<String, Image> createFinder() {
        return new Image.Finder<>(Image.class);
    }

    private String getImageFilenameById(String id) {
        return Play.application().configuration().getString("myUploadDir") + id;
    }

    private void moveFile(String from, String to) throws ParkingStarException {
        try {
            Path source = new File(from).toPath();
            Path target = new File(to).toPath();
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
            Files.delete(source);
        } catch (NoSuchFileException e0) {
            throw new ParkingStarFileException(ParkingStarFileException.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new ParkingStarFileException(ParkingStarFileException.UNABLE_TO_UPDATE);
        }
    }
}