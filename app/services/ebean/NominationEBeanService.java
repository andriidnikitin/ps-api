package services.ebean;

import di.SpringService;
import models.Nomination;
import models.User;
import models.Violator;
import org.springframework.stereotype.Repository;
import services.ImageService;
import services.NominationService;
import services.UserService;
import services.ViolatorService;
import services.exceptions.ParkingStarDatabaseException;
import services.exceptions.ParkingStarException;

import java.util.List;


@Repository
public class NominationEBeanService extends BaseEBeanService<Nomination, Integer> implements NominationService {

    @Override
    protected Class<models.Nomination> getEntityClass() {
        return models.Nomination.class;
    }

    @Override
    protected Integer getId(Nomination record) {
        return record.getId();
    }

    @Override
    public Nomination create(Nomination record) throws ParkingStarException {
        //TODO allow rollback
        UserService service = SpringService.getService(UserService.class);
        int nominatorId = record.getUser().getId();
        User user = service.read(nominatorId);
        if (user == null) {
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.RECORD_NOT_FOUND, nominatorId);
        }
        record.setViolator(SpringService.getService(ViolatorService.class).create(record.getViolator()));
        record.setImage(SpringService.getService(ImageService.class).create(record.getImage()));
        Nomination result = super.create(record);
        service.recalculatePoints(nominatorId, UserService.ADD_NOMINATION_CODE);
        result.setUser(user);
        return result;
    }

    @Override
    protected boolean recordExists(Nomination record) {
        List<Nomination> nominationsWithSameTimestamp = findByField("timestamp", record.getTimestamp());
        User user = record.getUser();
        List<Nomination> nominationsOfSameUser = findByField("user_id", user.getId());
        for (Nomination nomination : nominationsWithSameTimestamp) {
            if (nominationsOfSameUser.contains(nomination)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void update(Integer recordId, Nomination record) throws ParkingStarException {
        Nomination oldRecord = finder.byId(recordId);
        if (oldRecord == null)
            throw new ParkingStarDatabaseException(ParkingStarDatabaseException.RECORD_NOT_FOUND, recordId);
        record.setImage(oldRecord.getImage());
        super.update(recordId, record);
    }

    @Override
    public void delete(Integer recordId) throws ParkingStarException {
        int userId = finder.byId(recordId).getUser().getId();
        super.delete(recordId);
        SpringService.getService(UserService.class).recalculatePoints(userId, UserService.DELETE_NOMINATION_CODE);
    }

    @Override
    public Nomination findLastByViolatorId(int id) {
        List<Nomination> nominations = finder.where().eq(Violator.ID_FIELD_NAME, id).orderBy("timestamp").findList();
        return nominations.size() > 0 ? nominations.get(0) : null;
    }
}