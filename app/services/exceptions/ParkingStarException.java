package services.exceptions;

public abstract class ParkingStarException extends Exception {

    public ParkingStarException(String message) {
        super(message);
    }
}
