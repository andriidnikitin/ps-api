package services.exceptions;

public class ParkingStarFileException extends ParkingStarException {

    public static final String FILE_NOT_FOUND = "File not found";
    public static final String UNABLE_TO_DELETE = "Unable to delete.";
    public static final String UNABLE_TO_UPDATE = "Unable to update.";

    public ParkingStarFileException(String e) {
        super(e);
    }

}
