package services.exceptions;

import java.io.Serializable;

public class ParkingStarDatabaseException extends ParkingStarException {


    public static final String RECORD_NOT_FOUND = "Queried record not found.";
    public static final String ID_NOT_MATCH = "Provided id does not match id of provided record.";
    public static final String INVALID_RECORD = "Provided record is not in proper state";
    public static final String NO_RECORDS_PROVIDED = "Database does not contain any records of this type yet.";
    public static final String DUPLICATION = "Queried record is duplicated";
    public static final String DATABASE_ERROR = "Unable to create record";

    public ParkingStarDatabaseException(String type, Serializable recordId) {
        super(type + " ID: " + recordId);
    }

    public ParkingStarDatabaseException(String type) {
        super(type);
    }

}
