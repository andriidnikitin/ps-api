package services.exceptions;

public class ParkingStarAuthrizationException extends ParkingStarException {

    public final static String PASSWORD_NOT_MATCH = "Wrong password.";
    public static final String ALREADY_SIGNED_UP = "User with this login already exists.";

    public ParkingStarAuthrizationException(String s) {
        super(s);
    }

}
