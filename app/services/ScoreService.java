package services;

import models.Image;
import models.Nomination;
import models.ViolatorScore;
import services.exceptions.ParkingStarException;

import java.util.List;

public interface ScoreService {
    int getViolatorNominationsCount(int violatorId) throws ParkingStarException ;
    List<Nomination> getViolatorNominations(int violatorId) throws ParkingStarException;
    Image getViolatorImage(int violatorId) throws ParkingStarException;
    List<ViolatorScore> getScores() throws ParkingStarException;
    List<String> getNominators(int violatorId) throws ParkingStarException;
}
