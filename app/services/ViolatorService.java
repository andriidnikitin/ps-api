package services;

import models.Nomination;
import models.Violator;
import services.exceptions.ParkingStarException;

import java.util.List;

public interface ViolatorService extends IService<Violator, Integer> {

    List<Nomination> getNominations(int violatorId) throws ParkingStarException;
}
