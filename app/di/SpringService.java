package di;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"services"})
public class SpringService {
    private static BeanFactory factory;

    public static BeanFactory getFactory() {
        if (factory == null)
            factory = new AnnotationConfigApplicationContext(SpringService.class);
        return factory;
    }

    public static <R> R getService(Class<R> clazz) {
        try {
            return getFactory().getBean(clazz);
        } catch (BeansException e){
            throw new IllegalStateException("cannot provide service of class " + clazz, e);
        }
    }
}
