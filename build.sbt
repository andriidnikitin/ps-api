name := "play-java"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.6"

val org_springframework_ver = "4.1.6.RELEASE"

libraryDependencies ++= Seq(
  javaCore,
  javaWs % "test",
  "com.h2database" % "h2" % "1.4.181",
  "org.hibernate" % "hibernate-entitymanager" % "4.3.6.Final",
  "junit" % "junit" % "4.11",
  "com.google.code.maven-play-plugin.org.playframework.modules.db" % "play-db" % "1.1.1",
  //spring
  "org.springframework" % "spring-context" % org_springframework_ver,
  "org.springframework" % "spring-tx" % org_springframework_ver,
  "org.springframework" % "spring-expression" % org_springframework_ver,
  "org.springframework" % "spring-aop" % org_springframework_ver,
  "org.springframework" % "spring-test" % org_springframework_ver % "test",
  "org.springframework" % "spring-orm" % org_springframework_ver,
  "org.springframework" % "spring-jdbc" % org_springframework_ver,
  "org.springframework.data" % "spring-data-commons-core" % "1.4.1.RELEASE",
  //javax
  "javax.annotation" % "javax.annotation-api" % "1.2",
  "javax.inject" % "javax.inject" % "1",
  //json processing
  "com.google.code.gson" % "gson" % "2.3.1"

)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

inConfig(Test)(PlayEbean.scopedSettings)

playEbeanModels in Test := Seq("models.*")
